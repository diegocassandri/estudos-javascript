const mongosse = require('../database');
const bcrypt = require('bcryptjs');

const UserSchema = new mongosse.Schema({
    name: {
        type : String,
        required : true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true,
        select: false
    },
    createAt:{
        type: Date,
        default: Date.now
    }
});

UserSchema.pre('save', async function(next) {
    const hash  =  await bcrypt.hash(this.password,10);
    this.password = hash;
    
    next();
});

const User = mongosse.model('User', UserSchema);

module.exports = User;