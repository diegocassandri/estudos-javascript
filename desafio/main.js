/*Exercicio 1*/

function checarIdade(idade){
    return new Promise(function(resolve,reject){

        setTimeout(function() {
            if(idade >= 18){
                resolve();
            } else {
                reject()
            }
        },2000);
    });
}

checarIdade(40)
.then(() => {
    console.log('Maior que 18 anos!');
})
.catch(() => {
    console.log('Menor que 18 anos!');
});

var inputElement = document.querySelector('#app input');
var buttonElement = document.querySelector('#app button');
var listElement = document.querySelector('#app ul');
var userErrorMsg = document.querySelector('#app #userError');
var loadMsg = document.querySelector('#app #load');

console.log(userErrorMsg);

buttonElement.setAttribute('onclick','buscar()');

var repos = [];

function buscar(){
    while (listElement.hasChildNodes()){
        listElement.removeChild(listElement.firstChild);
    }
    var user = inputElement.value;

    loadMsg.style.display = 'inline';
    userErrorMsg.style.display = 'none'

    axios.get('https://api.github.com/users/' + user + '/repos')
    .then(function(response){
        var repos = response.data;
        for(repo of repos){
            var repoElement = document.createElement('li');
            var repoText = document.createTextNode(repo.name);

            repoElement.appendChild(repoText);
            listElement.appendChild(repoElement);
        }
        loadMsg.style.display = 'none';
    })
    .catch(function(error){
        userErrorMsg.style.display = 'inline'
        loadMsg.style.display = 'none';
    });

}